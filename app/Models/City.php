<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $fillable = ['name', 'population', 'zip_code'];

	public function parks()
    {
        return $this->hasMany('App\Models\Park');
    }
}
