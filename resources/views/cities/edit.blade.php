@extends('layouts.app')

@section('content')
<div class="container">
	<h1>Edit: {{$city->name}}</h1>

	<form method="POST" action="{{ route('cities.update', $city) }}">
	  @csrf
	  @method('PUT')
	  <div class="form-group">
	    <label for="name">Name</label>
	    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"  placeholder="City Name Here" name="name" value="{{ $city->name }}">
	    @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
	  </div>
	  <div class="form-group">
	    <label for="zip_code">Zip Code</label>
	    <input type="number" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" id="zip_code"  placeholder="Zip Code Here" name="zip_code" required value="{{ $city->zip_code }}">
	    @if ($errors->has('zip_code'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('zip_code') }}</strong>
            </span>
        @endif
	  </div>
	  <div class="form-group">
	    <label for="population">Population</label>
	    <input type="number" class="form-control{{ $errors->has('population') ? ' is-invalid' : '' }}" id="population" name="population" placeholder="Population Here" required value="{{ $city->population }}">
	    @if ($errors->has('population'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('population') }}</strong>
            </span>
        @endif
	  </div>
	  <button type="submit" class="btn btn-primary">Submit</button>
	</form>
</div>
@endsection