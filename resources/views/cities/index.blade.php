@extends('layouts.app')

@section('content')
<div class="container">
	<h1>Cities</h1>
	<a class="btn btn-success" href="{{ route('cities.create') }}">New City</a>
	@forelse ($cities as $city)
		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title">{{ $city->name }}</h5>
		    <h6 class="card-subtitle mb-2 text-muted">{{ $city->zip_code }}</h6>
		    <p class="card-text">{{ $city->population }}</p>
		    <a href="/cities/{{$city->id}}/edit " class="btn btn-warning">Edit</a>
		    <form method="POST" action="{{ route('cities.destroy', $city) }}">
		    	@csrf
	  			@method('DELETE')
		    	<button class="btn btn-danger">Delete</button>
		    </form>
		  </div>
		</div>
	@empty
	    <li>No data available</li>
	@endforelse
</div>
@endsection